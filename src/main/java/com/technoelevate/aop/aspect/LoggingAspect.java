package com.technoelevate.aop.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Aspect
@Component
@Slf4j
public class LoggingAspect {
	
	/*
	 * in the advice expression the first * represents returning type pattern 
	 * second * we have to specify package,3rd class, 4th method name, if we
	 * do not know the parameters inside the method use .. 
	 * Here we are applying logging for entire project
	 * if we want to apply for controller.EmployeeController.getEmployees()
	 */
	@Pointcut(value="execution (* com.technoelevate.aop.*.*.*(..))")
	public void loggingPointCut() {
		
	}
	
	@Around("loggingPointCut()")
	public Object loggingAdvice(ProceedingJoinPoint pj) throws Throwable {
		
		ObjectMapper mapper=new ObjectMapper();
		
		String methodName =pj.getSignature().getName();
		String className=pj.getTarget().getClass().toString();
		
		Object[] array=pj.getArgs();
		
		log.info("Inside "+className+"class "+methodName+" method,with request : "+mapper.writeValueAsString(array));
		
		Object response=pj.proceed();
		
		log.info("Inside "+className+"class "+methodName+" method,with response : "+mapper.writeValueAsString(array));

		return response;
		
	}

}
