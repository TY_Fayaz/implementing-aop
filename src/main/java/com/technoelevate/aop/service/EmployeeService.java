package com.technoelevate.aop.service;

import java.util.List;

import com.technoelevate.aop.entity.Employee;

public interface EmployeeService {

	public Employee addEmployee(Employee employee);

	public List<Employee> getEmployees();

	
}
