package com.technoelevate.aop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.technoelevate.aop.entity.Employee;
import com.technoelevate.aop.repository.EmployeeRepository;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeRepository repo;

	@Override
	public Employee addEmployee(Employee employee) {

		return repo.save(employee);
	}

	@Override
	public List<Employee> getEmployees() {

		return repo.findAll();
	}

}
