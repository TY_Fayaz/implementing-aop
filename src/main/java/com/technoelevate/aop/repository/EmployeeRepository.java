package com.technoelevate.aop.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.technoelevate.aop.entity.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer>{

}
